﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
namespace EProjectGroup2.Models
{
    public class EducationModel
    {
        [Key]
        public int EducationId { get; set; }
        public string? EducationName { get; set; }
        public string? ShortDescription { get; set; }
        public string? Description { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool Status { get; set; }
        public bool IsDelete { get; set; }
        public int? UserID { get; set; }
        public virtual UserModel? Users { get; set; }
    }
}
