﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace EProjectGroup2.Models.DataTransferObject.MedicalEquipmentModelDto
{
    public class AddMedicalEquipmentModelDto : IdentityUser<int>
    {
        public string? MedicalEquipmentName { get; set; }
        public int? MedicalEquipmentType { get; set; }
        public string? Description { get; set; }
        public int? Quantity { get; set; }
        public float? Price { get; set; }
        public string? ImagePath { get; set; }
        public IFormFile? ImageFile { get; set; }
        public DateTime? ImportDate { get; set; }
        public DateTime? ManufacturedDate { get; set; }
        public string? Manufacturer { get; set; }
        public bool Status { get; set; }
        public bool IsDelete { get; set; }
    }
}
