﻿using System.ComponentModel.DataAnnotations;

namespace EProjectGroup2.Models
{
    public class UserLogin
    {
        [Key]
        public int UserID { get; set; }
        [Required]
        public string? UserName { get; set; }
        [Required]
        public string? Password { get; set; }
       
    }
}
