﻿using EProjectGroup2.DataContext;
using EProjectGroup2.Models;
using EProjectGroup2.Models.DataTransferObject.UserModelDto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EProjectGroup2.Controllers
{
    public class UsersController : Controller
    {
        private readonly ClinicDataContext _clinicalDataContext;
        public UsersController(ClinicDataContext clinicDataContext)
        {
            _clinicalDataContext = clinicDataContext;
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            return _clinicalDataContext.Users != null ?
                        View(await _clinicalDataContext.Users.ToListAsync()) :
                        Problem("Entity set 'ClinicDataContext.Users'  is null.");
        }


        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Add(AddUserModelDto userModelDto)
        {
            var user = new UserModel()
            {
                UserName = userModelDto.UserName,
                Email = userModelDto.Email,
                Address = userModelDto.Address,
                Description = userModelDto.Description,
                DoB = userModelDto.DoB,
                FullName = userModelDto.FullName,
                IsAdmin = userModelDto.IsAdmin,
                IsDelete = userModelDto.IsDelete,
                Password = userModelDto.Password,
                Phone = userModelDto.Phone,
                Sex = userModelDto.Sex,
                Status = userModelDto.Status,
            };
            await _clinicalDataContext.Users.AddAsync(user);
            await _clinicalDataContext.SaveChangesAsync();
            return RedirectToAction("Add");
        }

        [HttpGet]
        public IActionResult Login()
        {
            //var usr = new UserLogin();
            //if (HttpContext.Request.Cookies["UsrName"] != null)
            //{
            //    usr.UserName = HttpContext.Request.Cookies["UsrName"];
            //}
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserLogin data)
        {
            UserLogin? usr = await _clinicalDataContext.UserLogins.FirstOrDefaultAsync(UserLogin => UserLogin.UserName == data.UserName && UserLogin.Password == data.Password);
            if (usr != null)
            {
                UserModel? us = await _clinicalDataContext.Users.FirstOrDefaultAsync(UserModel => UserModel.UserName == usr.UserName);
                if (us != null)
                {
                    ViewBag.username = string.Format("Successfully logged-in", data.UserName);
                    CookieOptions cookieOptions = new();
                    HttpContext.Response.Cookies.Append("UsrName", data.UserName, cookieOptions);
                    cookieOptions.Expires = new DateTimeOffset(DateTime.Now.AddDays(3));
                    if (us.IsAdmin == true)
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.username = string.Format("Login Failed", data.UserName);
                    return View();
                }
            }
            else
            {
                ViewBag.username = string.Format("Login Failed", data.UserName);
                return View();
            }
        }
    }
}
