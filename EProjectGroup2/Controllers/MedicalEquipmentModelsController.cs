﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;
using EProjectGroup2.Models.DataTransferObject.MedicalEquipmentModelDto;

namespace EProjectGroup2.Controllers
{
    public class MedicalEquipmentModelsController : Controller
    {
        private readonly ClinicDataContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;
        public MedicalEquipmentModelsController(ClinicDataContext context, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
        }

        // GET: MedicalEquipmentModels
        public async Task<IActionResult> Index()
        {
              return _context.MedicalEquipments != null ? 
                          View(await _context.MedicalEquipments.ToListAsync()) :
                          Problem("Entity set 'ClinicDataContext.MedicalEquipments'  is null.");
        }

        // GET: MedicalEquipmentModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.MedicalEquipments == null)
            {
                return NotFound();
            }

            var medicalEquipmentModel = await _context.MedicalEquipments
                .FirstOrDefaultAsync(m => m.MedicalEquipmentID == id);
            if (medicalEquipmentModel == null)
            {
                return NotFound();
            }

            return View(medicalEquipmentModel);
        }

        // GET: MedicalEquipmentModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: MedicalEquipmentModels/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddMedicalEquipmentModelDto model)
        {
            if (ModelState.IsValid)
            {
                //Save image to wwwroot/image
                string wwwRootPath = _hostEnvironment.WebRootPath;
                string fileName = Path.GetFileNameWithoutExtension(model.ImageFile.FileName);
                string extension = Path.GetExtension(model.ImageFile.FileName);
                model.ImagePath = fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                string path = Path.Combine(wwwRootPath + "/Image/", fileName);
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await model.ImageFile.CopyToAsync(fileStream);
                }

                var medicalEquipment = new MedicalEquipmentModel
                {
                    MedicalEquipmentName = model.MedicalEquipmentName,
                    MedicalEquipmentType = model.MedicalEquipmentType,
                    Description = model.Description,
                    Quantity = model.Quantity,
                    Price = model.Price,
                    ImagePath = model.ImagePath,
                    ImportDate = model.ImportDate,
                    ManufacturedDate = model.ManufacturedDate,
                    Manufacturer = model.Manufacturer,
                    Status = model.Status,
                    IsDelete = false
                };
                _context.Add(medicalEquipment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: MedicalEquipmentModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.MedicalEquipments == null)
            {
                return NotFound();
            }

            var medicalEquipmentModel = await _context.MedicalEquipments.FindAsync(id);
            if (medicalEquipmentModel == null)
            {
                return NotFound();
            }
            return View(medicalEquipmentModel);
        }

        // POST: MedicalEquipmentModels/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MedicalEquipmentID,MedicalEquipmentName,MedicalEquipmentType,Description,Quantity,Price,ImagePath,ImportDate,ManufacturedDate,Manufacturer,Status,IsDelete")] MedicalEquipmentModel medicalEquipmentModel)
        {
            if (id != medicalEquipmentModel.MedicalEquipmentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(medicalEquipmentModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MedicalEquipmentModelExists(medicalEquipmentModel.MedicalEquipmentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(medicalEquipmentModel);
        }

        // GET: MedicalEquipmentModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.MedicalEquipments == null)
            {
                return NotFound();
            }

            var medicalEquipmentModel = await _context.MedicalEquipments
                .FirstOrDefaultAsync(m => m.MedicalEquipmentID == id);
            if (medicalEquipmentModel == null)
            {
                return NotFound();
            }

            return View(medicalEquipmentModel);
        }

        // POST: MedicalEquipmentModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.MedicalEquipments == null)
            {
                return Problem("Entity set 'ClinicDataContext.MedicalEquipments'  is null.");
            }
            var medicalEquipmentModel = await _context.MedicalEquipments.FindAsync(id);
            if (medicalEquipmentModel != null)
            {
                _context.MedicalEquipments.Remove(medicalEquipmentModel);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MedicalEquipmentModelExists(int id)
        {
          return (_context.MedicalEquipments?.Any(e => e.MedicalEquipmentID == id)).GetValueOrDefault();
        }
    }
}
