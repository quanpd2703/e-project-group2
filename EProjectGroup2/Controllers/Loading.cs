﻿using Microsoft.AspNetCore.Mvc;

namespace EProjectGroup2.Controllers
{
    public class Loading : Controller
    {
        public async Task<IActionResult> Index()
        {
            await Task.Delay(3000);
            return View();
        }
    }
}
