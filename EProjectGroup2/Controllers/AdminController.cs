﻿using EProjectGroup2.DataContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EProjectGroup2.Controllers
{
    public class AdminController : Controller
    {
        private readonly ClinicDataContext _clinicalDataContext;
        public AdminController(ClinicDataContext clinicDataContext)
        {
            _clinicalDataContext = clinicDataContext;
        }
        public IActionResult Index()
        {

            return View();
        }

        public async Task<IActionResult> Users()
        {
            return _clinicalDataContext.Users != null ?
                        View(await _clinicalDataContext.Users.ToListAsync()) :
                        Problem("Entity set 'ClinicDataContext.Users'  is null.");
        }

        public async Task<IActionResult> Pharmacys()
        {
            return _clinicalDataContext.Users != null ?
                        View(await _clinicalDataContext.Pharmacies.ToListAsync()) :
                        Problem("Entity set 'ClinicDataContext.Pharmacy'  is null.");
        }

        public async Task<IActionResult> Educations()
        {
            return _clinicalDataContext.Users != null ?
                        View(await _clinicalDataContext.Educations.ToListAsync()) :
                        Problem("Entity set 'ClinicDataContext.Educations'  is null.");
        }

        public async Task<IActionResult> MedicalEquipments()
        {
            return _clinicalDataContext.Users != null ?
                        View(await _clinicalDataContext.MedicalEquipments.ToListAsync()) :
                        Problem("Entity set 'ClinicDataContext.MedicalEquipments'  is null.");
        }
    }
}
