﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EProjectGroup2.DataContext;
using EProjectGroup2.Models;

namespace EProjectGroup2.Controllers
{
    public class PharmacysController : Controller
    {
        private readonly ClinicDataContext _context;

        public PharmacysController(ClinicDataContext context)
        {
            _context = context;
        }

        // GET: PharmacyModels
        public async Task<IActionResult> Index()
        {
              return _context.Pharmacies != null ? 
                          View(await _context.Pharmacies.ToListAsync()) :
                          Problem("Entity set 'ClinicDataContext.Pharmacies'  is null.");
        }

        // GET: PharmacyModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Pharmacies == null)
            {
                return NotFound();
            }

            var pharmacyModel = await _context.Pharmacies
                .FirstOrDefaultAsync(m => m.PharmacyID == id);
            if (pharmacyModel == null)
            {
                return NotFound();
            }

            return View(pharmacyModel);
        }

        // GET: PharmacyModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PharmacyModels/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PharmacyID,PharmacyName,Description,Quantity,Price,ImagePath,ImportDate,ExpireDate,ManufacturedDate,Manufacturer,Status,IsDelete,Type")] PharmacyModel pharmacyModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(pharmacyModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(pharmacyModel);
        }

        // GET: PharmacyModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Pharmacies == null)
            {
                return NotFound();
            }

            var pharmacyModel = await _context.Pharmacies.FindAsync(id);
            if (pharmacyModel == null)
            {
                return NotFound();
            }
            return View(pharmacyModel);
        }

        // POST: PharmacyModels/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PharmacyID,PharmacyName,Description,Quantity,Price,ImagePath,ImportDate,ExpireDate,ManufacturedDate,Manufacturer,Status,IsDelete,Type")] PharmacyModel pharmacyModel)
        {
            if (id != pharmacyModel.PharmacyID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(pharmacyModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PharmacyModelExists(pharmacyModel.PharmacyID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(pharmacyModel);
        }

        // GET: PharmacyModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Pharmacies == null)
            {
                return NotFound();
            }

            var pharmacyModel = await _context.Pharmacies
                .FirstOrDefaultAsync(m => m.PharmacyID == id);
            if (pharmacyModel == null)
            {
                return NotFound();
            }

            return View(pharmacyModel);
        }

        // POST: PharmacyModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Pharmacies == null)
            {
                return Problem("Entity set 'ClinicDataContext.Pharmacies'  is null.");
            }
            var pharmacyModel = await _context.Pharmacies.FindAsync(id);
            if (pharmacyModel != null)
            {
                _context.Pharmacies.Remove(pharmacyModel);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PharmacyModelExists(int id)
        {
          return (_context.Pharmacies?.Any(e => e.PharmacyID == id)).GetValueOrDefault();
        }
    }
}
