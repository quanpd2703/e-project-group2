﻿using EProjectGroup2.Models;
using Microsoft.EntityFrameworkCore;

namespace EProjectGroup2.DataContext
{
    public class ClinicDataContext : DbContext
    {
        public ClinicDataContext(DbContextOptions options) : base(options)
        {
        }
        
        public DbSet<UserModel> Users { get; set; }
        public DbSet<PharmacyModel> Pharmacies { get; set; }
        public DbSet<MedicalEquipmentModel> MedicalEquipments { get; set; }
        public DbSet<EducationModel> Educations { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<OrdersModel> Orders { get; set; }
        public DbSet<ReviewsModel> Reviews { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<UserLogin>().ToTable("UserLogins");
            builder.Entity<UserLogin>().HasKey(t => t.UserID);

            builder.Entity<UserModel>().ToTable("Users");
            builder.Entity<UserModel>().HasKey(t=>t.UserId);

            builder.Entity<PharmacyModel>().ToTable("Pharmacies");
            builder.Entity<PharmacyModel>().HasKey(t => t.PharmacyID);

            builder.Entity<MedicalEquipmentModel>().ToTable("MedicalEquipments");
            builder.Entity<MedicalEquipmentModel>().HasKey(t => t.MedicalEquipmentID);

            builder.Entity<EducationModel>().ToTable("Educations");
            builder.Entity<EducationModel>().HasKey(t => t.EducationId);
            builder.Entity<EducationModel>()
                .HasOne(a=>a.Users)
                .WithMany(x=>x.Educations)
                .HasForeignKey(y=>y.UserID)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.Entity<OrdersModel>().ToTable("Orders");
            builder.Entity<OrdersModel>().HasKey(t => t.OrderID);
            builder.Entity<OrdersModel>(entity =>
            {
                entity.HasOne(a => a.Users)
                    .WithMany(x => x.Orders)
                    .HasForeignKey(y => y.UserID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
                entity.HasOne(a => a.Pharmacies)
                    .WithMany(x => x.Orders)
                    .HasForeignKey(y => y.PharmacyID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
                entity.HasOne(a => a.MedicalEquipments)
                    .WithMany(x => x.Orders)
                    .HasForeignKey(y => y.MedicalEquipmentID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
            });

            builder.Entity<ReviewsModel>().ToTable("Reviews");
            builder.Entity<ReviewsModel>().HasKey(t => t.ReviewID);
            builder.Entity<ReviewsModel>(entity =>
            {
                entity.HasOne(a => a.Users)
                    .WithMany(x => x.Reviews)
                    .HasForeignKey(y => y.UserID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
                entity.HasOne(a => a.Pharmacies)
                    .WithMany(x => x.Reviews)
                    .HasForeignKey(y => y.PharmacyID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
                entity.HasOne(a => a.MedicalEquipments)
                    .WithMany(x => x.Reviews)
                    .HasForeignKey(y => y.MedicalEquipmentID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired();
            });
            builder.Entity<UserLogin>().HasData(
                new UserLogin {
                    UserID = 1 ,
                    UserName="admin",
                    Password="admin"
                },
                new
                {
                    UserID = 2,
                    UserName = "trahoangan",
                    Password = "trahoangan123456"
                },
                new
                {
                    UserID = 3,
                    UserName = "quangminhan",
                    Password = "quangminhan123456"
                },
                new
                {
                    UserID = 4,
                    UserName = "lieugiakiet",
                    Password = "lieugiakiet123456"
                },
                new
                {
                    UserID = 5,
                    UserName = "thaoxuankhoa",
                    Password = "thaoxuankhoa123456"
                },
                new
                {
                    UserID = 6,
                    UserName = "trieuphuhiep",
                    Password = "trieuphuhiep123456"
                },
                new
                {
                    UserID = 7,
                    UserName = "cominhhoa",
                    Password = "cominhhoa123456"
                },
                new
                {
                    UserID = 8,
                    UserName = "buigiahoang",
                    Password = "buigiahoang123456"
                },
                new
                {
                    UserID = 9,
                    UserName = "lylyngochuy",
                    Password = "lylyngochuy123456"
                },
                new
                {
                    UserID = 10,
                    UserName = "khaquanglinh",
                    Password = "khaquanglinh123456"
                },
                new
                {
                    UserID = 11,
                    UserName = "hoatungminh",
                    Password = "hoatungminh123456"
                }
                );
            builder.Entity<UserModel>().HasData(
                new {
                    UserId = 1, 
                    UserName = "admin", 
                    Password = "admin", 
                    FullName="admin",
                    Sex="male",
                    Phone="12345678",
                    DoB=new DateTime(2000,01,01),
                    Address="Ha Noi",
                    Description="None",
                    Email="abc@gmail.com",
                    IsAdmin=true,
                    IsDelete=false,
                    Status=true
                },
                new
                {
                    UserId = 2,
                    UserName = "trahoangan",
                    Password = "trahoangan123456",
                    FullName = "Trà Hoàng Ân",
                    Sex = "male",
                    Phone = "0586072996",
                    DoB = new DateTime(2003, 01, 01),
                    Address = "Ha Noi",
                    Description = "None",
                    Email = "trahoangan@gmail.com",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true
                },
                new
                {
                    UserId = 3,
                    UserName = "quangminhan",
                    Password = "quangminhan123456",
                    FullName = "Quảng Minh Ân",
                    Sex = "male",
                    Phone = "0586037708",
                    DoB = new DateTime(2002, 11, 30),
                    Address = "Hai Phong",
                    Description = "None",
                    Email = "quangminhan123456",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true
                },
                new
                {
                    UserId = 4,
                    UserName = "lieugiakiet",
                    Password = "lieugiakiet123456",
                    FullName = "Liễu Gia Kiệt",
                    Sex = "male",
                    Phone = "0586073704",
                    DoB = new DateTime(2004, 01, 01),
                    Address = "Da Nang",
                    Description = "None",
                    Email = "lieugiakiet@gmail.com",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true
                },
                new
                {
                    UserId = 5,
                    UserName = "thaoxuankhoa",
                    Password = "thaoxuankhoa123456",
                    FullName = "Thào Xuân Khoa",
                    Sex = "male",
                    Phone = "0586512274",
                    DoB = new DateTime(2001, 03, 10),
                    Address = "Ha Noi",
                    Description = "None",
                    Email = "thaoxuankhoa@gmail.com",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true
                },
                new
                {
                    UserId = 6,
                    UserName = "trieuphuhiep",
                    Password = "trieuphuhiep123456",
                    FullName = "Triệu Phú Hiệp",
                    Sex = "male",
                    Phone = "0586935079",
                    DoB = new DateTime(2003, 10, 05),
                    Address = "Ha Noi",
                    Description = "None",
                    Email = "trieuphuhiep@gmail.com",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true
                },
                new
                {
                    UserId = 7,
                    UserName = "cominhhoa",
                    Password = "cominhhoa123456",
                    FullName = "Cổ Minh Hòa",
                    Sex = "female",
                    Phone = "0586484874",
                    DoB = new DateTime(2003, 05, 28),
                    Address = "Hai Phong",
                    Description = "None",
                    Email = "cominhhoa@gmail.com",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true
                },
                new
                {
                    UserId = 8,
                    UserName = "buigiahoang",
                    Password = "buigiahoang123456",
                    FullName = "Bùi Gia Hoàng",
                    Sex = "famale",
                    Phone = "0367880706",
                    DoB = new DateTime(2000, 01, 01),
                    Address = "Ha Noi",
                    Description = "None",
                    Email = "buigiahoang@gmail.com",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true
                },
                new
                {
                    UserId = 9,
                    UserName = "lyngochuy",
                    Password = "lyngochuy123456",
                    FullName = "Lý Ngọc Huy",
                    Sex = "male",
                    Phone = "0367778384",
                    DoB = new DateTime(2003, 04, 29),
                    Address = "Ha Noi",
                    Description = "None",
                    Email = "lyngochuy@gmail.com",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true
                },
                new
                {
                    UserId = 10,
                    UserName = "khaquanglinh",
                    Password = "khaquanglinh123456",
                    FullName = "Khà Quang Linh",
                    Sex = "male",
                    Phone = "0586484756",
                    DoB = new DateTime(2003, 11, 02),
                    Address = "Ha Noi",
                    Description = "None",
                    Email = "khaquanglinh@gmail.com",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true
                },
                new
                {
                    UserId = 11,
                    UserName = "hoatungminh",
                    Password = "hoatungminh123456",
                    FullName = "Hoa Tùng Minh",
                    Sex = "male",
                    Phone = "0367880705",
                    DoB = new DateTime(2003, 11, 05),
                    Address = "Ha Noi",
                    Description = "None",
                    Email = "hoatungminh@gmail.com",
                    IsAdmin = false,
                    IsDelete = false,
                    Status = true

                }
                );
            builder.Entity<PharmacyModel>().HasData(
                new PharmacyModel
                {
                    PharmacyID = 1,
                    PharmacyName="Paradon",
                    Quantity=1,
                    Price=1,
                    Description= "anadol medicine contains paracetamol which is an active pain reliever and fever reducer. It is effective in reducing fever and alleviating mild to moderate pain symptoms such as: Headache, Migraine,Muscle pain, osteoarthritis pain,Dysmenorrhea,Sore throat,Musculoskeletal pain, pain caused by osteoarthritis,Fever and pain after vaccination,Toothache, pain after tooth extraction or dental surgery",
                    ExpireDate=new DateTime(2023,12,12),
                    ImportDate=new DateTime(2022,12,12),
                    ImagePath=null,
                    ManufacturedDate= new DateTime(2023, 02, 12),
                    Manufacturer="DMP",
                    IsDelete=false,
                    Status=true,
                    Type= "Antipyretics"

                },
                new PharmacyModel
                {
                    PharmacyID = 2,
                    PharmacyName = "Lidocain",
                    Quantity = 40,
                    Price = 123900,
                    Description = "Lidocaine is widely known as a common anesthetic. It is also used in the treatment of dangerous acute arrhythmias.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "Egis Pharmaceuticals PLC",
                    IsDelete = false,
                    Status = true,
                    Type = "Anesthesia"

                },
                new PharmacyModel
                {
                    PharmacyID = 3,
                    PharmacyName = "Atropin sulfat",
                    Quantity = 10,
                    Price = 500,
                    Description = "Atropin sulfate is a medicine used to treat slow heart rate and symptoms of pesticide poisoning. What are the uses of Gardenal 10mg to better understand? What are the side effects of atropine sulfate? What is the correct way to drink? What points should be kept in mind when using? The following article will help you better understand the drug Atropin sulfate.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "Vinphaco",
                    IsDelete = false,
                    Status = true,
                    Type = "Prescription drugs"

                },
                new PharmacyModel
                {
                    PharmacyID = 4,
                    PharmacyName = "Alfuzosin",
                    Quantity = 10,
                    Price = 500,
                    Description = "Medicines to treat prostate symptoms",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "Remedica Ltd.",
                    IsDelete = false,
                    Status = true,
                    Type = "Prescription drugs"

                },
                new PharmacyModel
                {
                    PharmacyID = 5,
                    PharmacyName = "Diltiazem",
                    Quantity = 10,
                    Price = 1600,
                    Description = "Medications to treat high blood pressure",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "Remedica Ltd",
                    IsDelete = false,
                    Status = true,
                    Type = "Prescription drugs"

                },
                new PharmacyModel
                {
                    PharmacyID = 6,
                    PharmacyName = "Naproxen",
                    Quantity = 10,
                    Price = 500,
                    Description = "Naproxen is used to relieve pain for medical conditions such as headaches, muscle aches, tendonitis, toothaches, and menstrual cramps. It also helps relieve pain, swelling and stiffness caused by arthritis, bursitis, and gout.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "Remedica Ltd.",
                    IsDelete = false,
                    Status = true,
                    Type = "Prescription drugs"

                },
                new PharmacyModel
                {
                    PharmacyID = 7,
                    PharmacyName = "Allopurinol",
                    Quantity = 10,
                    Price = 500,
                    Description = "Allopurinol is used to treat gout and certain types of kidney stones. It is also used to prevent increased uric acid levels in patients receiving cancer chemotherapy. These patients can have increased uric acid levels due to release of uric acid from the dying cancer cells.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "Remedica Ltd.",
                    IsDelete = false,
                    Status = true,
                    Type = "Prescription drugs"

                },
                new PharmacyModel
                {
                    PharmacyID = 8,
                    PharmacyName = "Cefoperazone",
                    Quantity = 10,
                    Price = 500,
                    Description = "Cefoperazone is a broad-spectrum cephalosporin antibiotic used for the treatment of bacterial infections in various locations, including the respiratory tract, abdomen, skin, and female genital tracts.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "Medochemie Ltd. - Factory C",
                    IsDelete = false,
                    Status = true,
                    Type = "Prescription drugs"

                },
                new PharmacyModel
                {
                    PharmacyID = 9,
                    PharmacyName = "Cefoxitin",
                    Quantity = 10,
                    Price = 500,
                    Description = "Cefoxitin injection is used to treat infections caused by bacteria including pneumonia and other lower respiratory tract (lung) infections; and urinary tract, abdominal (stomach area), female reproductive organs, blood, bone, joint, and skin infections.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "LDP Laboratorios Torlan SA",
                    IsDelete = false,
                    Status = true,
                    Type = "Prescription drugs"

                },
                new PharmacyModel
                {
                    PharmacyID = 10,
                    PharmacyName = "Canxi Calcium Corbiere Extra",
                    Quantity = 10,
                    Price = 500,
                    Description = "Calcium Corbiere Extra oral solution is a product of Sanofi Pharmaceutical Joint Stock Company containing active ingredients Calcium glubionate and Calcium lactobionate used to supplement calcium in the treatment of osteoporosis, calcium deficiency, especially in cases of need High calcium requirements such as: Pregnant and lactating women, growing children, recovery period, fractures, rickets.",
                    ExpireDate = new DateTime(2023, 12, 12),
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "Viet Nam",
                    IsDelete = false,
                    Status = true,
                    Type = "Prescription drugs"

                }
                );
            builder.Entity<MedicalEquipmentModel>().HasData(
                new MedicalEquipmentModel
                {
                    MedicalEquipmentID = 1,
                    MedicalEquipmentName = "Paradon",
                    Quantity = 1,
                    Price = 1,
                    Description = "None",
                    MedicalEquipmentType=01,
                    ImportDate = new DateTime(2022, 12, 12),
                    ImagePath = null,
                    ManufacturedDate = new DateTime(2023, 02, 12),
                    Manufacturer = "DMP",
                    IsDelete = false,
                    Status = true

                }
                );
            builder.Entity<EducationModel>().HasData(
                new {
                    EducationId = 1,
                    EducationName="School",
                    ShortDescription="None",
                    Description="None",
                    CreatedDate=new DateTime(2023,01,01),
                    IsDelete=false,
                    Status=true,
                    UserID=1
                },
                new
                {
                    EducationId = 2,
                    EducationName = "Alliance for Clinical Education",
                    ShortDescription = "The Alliance for Clinical Education’s mission is to foster collaboration across specialties to promote excellence in clinical education of medical students. ",
                    Description = "The Alliance for Clinical Education’s mission is to foster collaboration across specialties to promote excellence in clinical education of medical students. In support of its mission, the Alliance for Clinical Education will take a national leadership role in promoting:\n-The recognition of excellent medical education as the enduring core mission of medical schools with a concomitant commitment of adequate resources.\n-Collaboration, communication, and resource sharing among educators in different clinical disciplines, including a multidisciplinary approach to core competencies for medical students.\n-Innovation in medical student education based on well-designed research, especially as this applies across clinical disciplines",
                    CreatedDate = new DateTime(2023, 02, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                },
                new
                {
                    EducationId = 3,
                    EducationName = "American Board of Internal Medicine",
                    ShortDescription = "NoABIM is a physician-led, non-profit, independent evaluation organization driven by doctors who want to achieve higher standards for better care in a rapidly changing world.ne",
                    Description = "Since its founding in 1936 to answer a public call to establish more uniform standards for physicians, Certification by the American Board of Internal Medicine (ABIM) has stood for the highest standard in internal medicine and its 20 subspecialties. Certification has meant that internists have demonstrated – to their peers and to the public – that they have the clinical judgment, skills and attitudes essential for the delivery of excellent patient care.\nABIM is not a membership society, but a physician-led, non-profit, independent evaluation organization. Our accountability is both to the profession of medicine and to the public.",
                    CreatedDate = new DateTime(2023, 03, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                },
                new
                {
                    EducationId = 4,
                    EducationName = "American Medical Association (AMA)",
                    ShortDescription = "The AMA’s system of governance and policy making include the board of trustees, House of Delegates, executive vice president, councils and committees, special sections, and AMA senior leadership and staff.",
                    Description = "Founded in 1847, the American Medical Association (AMA) is the largest and only national association that convenes 190+ state and specialty medical societies and other critical stakeholders. Throughout history, the AMA has always followed its mission: to promote the art and science of medicine and the betterment of public health./nAs the physicians’ powerful ally in patient care, the AMA delivers on this mission by representing physicians with a unified voice in courts and legislative bodies across the nation, removing obstacles that interfere with patient care, leading the charge to prevent chronic disease and confront public health crises, and driving the future of medicine to tackle the biggest challenges in health care and training the leaders of tomorrow.\nThe AMA’s system of governance and policy making include the board of trustees, House of Delegates, executive vice president, councils and committees, special sections, and AMA senior leadership and staff.",
                    CreatedDate = new DateTime(2023, 04, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                },
                new
                {
                    EducationId = 5,
                    EducationName = "Association of American Medical Colleges",
                    ShortDescription = "The AAMC leads and serves the academic medicine community to improve the health of people everywhere. Founded in 1876 and based in Washington, D.C., the AAMC is a not-for-profit association dedicated to transforming health through medical education, health care, medical research, and community collaborations.",
                    Description = "The AAMC leads and serves the academic medicine community to improve the health of people everywhere. Founded in 1876 and based in Washington, D.C., the AAMC is a not-for-profit association dedicated to transforming health through medical education, health care, medical research, and community collaborations.",
                    CreatedDate = new DateTime(2023, 05, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                },
                new
                {
                    EducationId = 6,
                    EducationName = "Generalists in Medical Education",
                    ShortDescription = "The Generalists in Medical Education welcome basic scientists, clinicians, and other educators interested in medical education.",
                    Description = "The Generalists in Medical Education welcome basic scientists, clinicians, and other educators interested in medical education. We are educators who teach, conduct research, and provide support services in all areas of predoctoral, postdoctoral and continuing medical education and health professions education. Specific areas of interest include curriculum and faculty development, testing and evaluation, and student services. At each annual conference, we exchange ideas and knowledge to enhance our professional growth. Each conference offers opportunities to develop understandings of the latest initiatives and innovations in medical education and to explore solutions to educational problems.",
                    CreatedDate = new DateTime(2023, 06, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                },
                new
                {
                    EducationId = 7,
                    EducationName = "National Board of Medical Examiners",
                    ShortDescription = "NBME is a mission-driven organization that specializes in the creation of high-quality assessments and learning tools.",
                    Description = "Over 100 years later, NBME strives not only to ensure a higher standard of qualification for medical practice, but also to protect the health of the public through state-of-the-art assessment of health professionals.\\n\r\nUnder the leadership of our current Board of Directors, we hope to fulfill this mission through continuous improvement and collaboration with medical schools, health profession organizations and members of the public.",
                    CreatedDate = new DateTime(2023, 07, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                },
                new
                {
                    EducationId = 8,
                    EducationName = "Educational Commission for Foreign Medical Graduates",
                    ShortDescription = "The Educational Commission for Foreign Medical Graduates (ECFMG) is a member of Intealth , an integrated organization that also includes the Foundation for Advancement of International Medical Education and Research (FAIMER®) ",
                    Description = "The Educational Commission for Foreign Medical Graduates (ECFMG) is a member of Intealth , an integrated organization that also includes the Foundation for Advancement of International Medical Education and Research (FAIMER®) . Through strategic integration of its nonprofit members, Intealth brings together the expertise and resources that advance quality in health care education worldwide in order to improve health care for all. As members of Intealth, ECFMG and FAIMER share a common vision and pursue complementary missions. Read more about our Vision, Missions & Values .",
                    CreatedDate = new DateTime(2023, 08, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                },
                new
                {
                    EducationId = 9,
                    EducationName = "Association of Standardized Patient Educators (ASPE)",
                    ShortDescription = "ASPE is the international organization of educators dedicated to human simulation",
                    Description = "ASPE is the international organization of educators dedicated to human simulation through:\n-Promoting best practices in the application of SP methodology for education, assessment and research\n-Fostering the dissemination of research and scholarship in the field of SP methodology\n-Advancing the professional knowledge and skills of its members and affiliates",
                    CreatedDate = new DateTime(2023, 01, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                },
                new
                {
                    EducationId = 10,
                    EducationName = "International Association of Medical Science Educators",
                    ShortDescription = "IAMSE is a nonprofit professional development society organized and directed by health professions educators whose goals include promoting excellence and innovation in teaching, student assessment, program evaluation, instructional technology, human simulation, and learner-centered education.",
                    Description = "IAMSE is a nonprofit professional development society organized and directed by health professions educators whose goals include promoting excellence and innovation in teaching, student assessment, program evaluation, instructional technology, human simulation, and learner-centered education.",
                    CreatedDate = new DateTime(2023, 10, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                },
                new
                {
                    EducationId = 11,
                    EducationName = "Society for Simulation in Healthcare",
                    ShortDescription = "Simulation is the imitation or representation of one act or system by another.? healthcare simulations can be said to have four main purposes – education, assessment, research, and health system integration in facilitating patient safety.",
                    Description = "Simulation education is a bridge between classroom learning and real-life clinical experience. Novices – and patients - may learn how to do injections by practicing on an orange with a real needle and syringe. Much more complex simulation exercises – similar to aviation curricula that provided the basis for healthcare – may rely on computerized mannequins that perform dozens of human functions realistically in a healthcare setting such as an operating room or critical care unit that is indistinguishable from the real thing. Whether training in a “full mission environment” or working with a desk top virtual reality machine that copies the features of a risky procedure, training simulations do not put actual patients at risk. Healthcare workers are subject to unique risks in real settings too, from such things as infected needles, knife blades and other sharps as well as electrical equipment, and they are also protected during simulations that allow them to perfect their craft.",
                    CreatedDate = new DateTime(2023, 11, 01),
                    IsDelete = false,
                    Status = true,
                    UserID = 1
                }
                );
            builder.Entity<OrdersModel>().HasData(
                new OrdersModel
                {
                    OrderID=1,
                    CreatedDate = new DateTime(2022,01,01),
                    MedicalEquipmentID=1,
                    PharmacyID=1,
                    UserID=1
                },
                new
                {
                    OrderID = 2,
                    CreatedDate = new DateTime(2022, 02, 04),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    OrderID = 3,
                    CreatedDate = new DateTime(2022, 10, 05),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    OrderID = 4,
                    CreatedDate = new DateTime(2022, 11, 07),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    OrderID = 5,
                    CreatedDate = new DateTime(2022, 12, 06),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    OrderID = 6,
                    CreatedDate = new DateTime(2022, 12, 08),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    OrderID = 7,
                    CreatedDate = new DateTime(2022, 08, 05),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    OrderID = 8,
                    CreatedDate = new DateTime(2022, 07, 07),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                }
                );
            builder.Entity<ReviewsModel>().HasData(
                new ReviewsModel
                {
                    ReviewID = 1,
                    CreatedDate = new DateTime(2022, 01, 01),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    ReviewID = 2,
                    CreatedDate = new DateTime(2021, 12, 08),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    ReviewID = 3,
                    CreatedDate = new DateTime(2022, 07, 04),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    ReviewID = 4,
                    CreatedDate = new DateTime(2022, 06, 05),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    ReviewID = 5,
                    CreatedDate = new DateTime(2022, 02, 01),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    ReviewID = 6,
                    CreatedDate = new DateTime(2022, 03, 10),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    ReviewID = 7,
                    CreatedDate = new DateTime(2022, 04, 11),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    ReviewID = 8,
                    CreatedDate = new DateTime(2022, 03, 02),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                },
                new
                {
                    ReviewID = 9,
                    CreatedDate = new DateTime(2022, 12, 05),
                    MedicalEquipmentID = 1,
                    PharmacyID = 1,
                    UserID = 1
                }
                );

        }

    }
}
